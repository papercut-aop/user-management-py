#!/usr/bin/env python

# Dataclass to represent a user

# For an introduction to Pythjon Dataclasses see https://youtu.be/CvQ7e6yUtnw

from dataclasses import asdict, dataclass, field, InitVar, asdict
from dataclasses_json import dataclass_json

@dataclass_json
@dataclass
class User:
    inviteText : InitVar[str] = "Default API invite"
    userName : InitVar[str] = ''
    externalId : InitVar[str] = ''
    email : InitVar[str] = ''
    formattedName : InitVar[str] = ''
    invitation : dict = field(init=False, default_factory=dict)
    user : dict = field(init=False, default_factory=dict)

    def __post_init__(self, inviteText, userName, externalId, email, formattedName):
        self.invitation['message'] = inviteText
        self.user["externalId"] = externalId
        self.user["userName"] = userName
        self.user['email'] = [{'type': "work", 'primary' : True, 'value': email}]
        self.user['name'] = {'formatted': formattedName}


if __name__ == "__main__":
    u2 = User(inviteText = "Invite here", userName="alecc", email="alec.clews@papercut.com",
                externalId="1234", formattedName="Alec The Geek")
    print(asdict(u2))

#!/usr/bin/env python

# A simple example of PaperCut Cloud Native Add-On implementation

# Note this is not production code. It's designed to show what needs to be implemented in a simple manner

from requests import post, get
from logging.config import dictConfig
from logging import critical, info, debug, exception, basicConfig, DEBUG
from json import load as jsonload
from typer import Typer, Option


from aop_lib.ourConfig import getConfig
from aop_lib.ourDatabase import getCustomerList
from AOP_APIwrappers import inviteUser, deleteUser
from user import User

manageUsers = Typer()

basicConfig( level=DEBUG)

ourConfig = getConfig("UserManagement")  # hard coded because that is all this program does


@manageUsers.command()
def invite( userName: str = Option(...),
            formattedName: str = Option(...),
            email: str = Option(...),
            externalID: str = Option(...),
            inviteText: str="API Invited you"):


    orgList = getCustomerList(u'UserManagement', ourConfig['firebase-info'])

    user = User(inviteText = inviteText, userName=userName, email=email,
                externalId=externalID, formattedName=formattedName)

    for org in orgList:

        debug(f"current org being processed is {org}")

        inviteUser(org=org['orgId'], productID=u'UserManagement', userInfo=user)

@manageUsers.command()
def remove(userName: str, email: str, externalID: str):

    orgList = getCustomerList(u'UserManagement', ourConfig['firebase-info'])

    user = User(userName=userName, email=email,
                externalId=externalID)

    for org in orgList:

        debug(f"current org being processed is {org}")

        deleteUser(org=org['orgId'], productID=u'UserManagement', userInfo=user)

if __name__ == "__main__":
    manageUsers()

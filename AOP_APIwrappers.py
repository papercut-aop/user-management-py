#!/usr/bin/env python

import json
from requests import post, codes, get, delete, put
from logging import debug, basicConfig, error, info, DEBUG, exception
from urllib.parse import quote

from aop_lib.ourConfig import getConfig
from aop_lib.manageAccessTokens import getAccessToken

from user import User

from sys import exit

basicConfig(level=DEBUG)

from enum import Enum

class queryFieldType(Enum):
    externalId = 'externalId'
    email = 'email.value'
    username = 'username'

def getAPIinfo(productID):
    return getConfig(productID).get("api-version"), getConfig(productID).get('api-root') 

def inviteUser(org: str, productID: str, userInfo: User):

    apiVersion, apiRoot =  getAPIinfo(productID=productID)

    debug(f"About to call Invite User for user {userInfo}")

    payload = userInfo.to_json()

    resp = post( f"{apiRoot}/{org}/user-management/{apiVersion}/invitations",
            headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Accept": "application/json"},
            data=payload
            )

    debug(f"called API user-management.invitations and response was {resp}")
    
    if not resp.ok :
        exception( f"API Call user-management.invitations failed for org {org}, \n{resp}")
        if resp.status_code == 403:
            error(f"org {org} is no longer a customer")
            return None
        else:
            resp.raise_for_status()

    return resp.json()



def queryUser(org: str, productID: str, queryField: queryFieldType, queryValue: str) -> User:
    apiVersion, apiRoot =  getAPIinfo(productID=productID)

    debug(f"About to call Query User for user {queryValue}")

    queryStr = quote("filter=" + queryField + " eq " + queryValue)

    resp = get( f"{apiRoot}/{org}/scim/{apiVersion}/Users?{queryStr}",
            headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Accept": "application/json"}
            )

    if not resp.ok :
        exception( f"API Call user-management.queryUser failed for org {org}, query String {queryStr}\n{resp}")
        if resp.status_code == 403:
            error(f"org {org} is no longer a customer")
            return None
        else:
            resp.raise_for_status()

    return User.from_json(resp.json())

def getUser(org: str, productID: str, userId: str) -> User:

    apiVersion, apiRoot =  getAPIinfo(productID=productID)

    debug(f"About to call get User for user {userId}")

    resp = get( f"{apiRoot}/{org}/scim/{apiVersion}/Users/{userId}",
            headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Accept": "application/json"}
            )

    if not resp.ok :
        exception( f"API Call user-management.getUser failed for org {org}, user ID {userId}\n{resp}")
        if resp.status_code == 403:
            error(f"org {org} is no longer a customer")
            return None
        else:
            resp.raise_for_status()

    return User.from_json(resp.json())

def updateUser(org: str, productID: str, userId: str, userInfo: User) -> json:

    apiVersion, apiRoot =  getAPIinfo(productID=productID)

    debug(f"About to call delete User for user {userId}")

    resp = put( f"{apiRoot}/{org}/scim/{apiVersion}/Users/{userId}",
            headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Accept": "application/json"},
            data = userInfo
            )

    if not resp.ok :
        exception( f"API Call user-management.deleteUser failed for org {org}, user ID {userId}\n{resp}")
        if resp.status_code == 403:
            error(f"org {org} is no longer a customer")
            return None
        else:
            resp.raise_for_status()

    return resp.json()


def deleteUser(org: str, productID: str, userId: str) -> json:

    apiVersion, apiRoot =  getAPIinfo(productID=productID)

    debug(f"About to call delete User for user {userId}")

    resp = delete( f"{apiRoot}/{org}/scim/{apiVersion}/Users/{userId}",
            headers={
                "Authorization": f"Bearer {getAccessToken(productID)}",
                "Accept": "application/json"}
            )

    if not resp.ok :
        exception( f"API Call user-management.deleteUser failed for org {org}, user ID {userId}\n{resp}")
        if resp.status_code == 403:
            error(f"org {org} is no longer a customer")
            return None
        else:
            resp.raise_for_status()

    return resp.json()

